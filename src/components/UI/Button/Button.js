import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = (props) => {
    return (
        <button
            disabled={props.disabled}
            className={[styles.Button, styles[props.btnType]].join(' ')}
            onClick={props.onButtonClicked}>
            {props.children}
        </button>
    )
}

Button.propTypes = {
    disabled: PropTypes.bool,
    btnType: PropTypes.string,
    onButtonClicked: PropTypes.func
}

export default Button;