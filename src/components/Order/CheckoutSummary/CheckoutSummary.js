import React from 'react';
import styles from './CheckoutSummary.module.css';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

const ChekoutSummary = (props) => {
    return (
        <div className={styles.Summary}>
            <h1>We hope it tastes well!</h1>
            <div className={styles.Burger}>
                <Burger ingredients={props.ingredients} />
            </div>

            <Button 
                onButtonClicked={props.onCheckoutCancelled}
                btnType="Danger">CANCEL</Button>
            <Button 
                onButtonClicked={props.onCheckoutContinued}
                btnType="Success">CONTINUE</Button>
        </div>
    )
}

export default ChekoutSummary;