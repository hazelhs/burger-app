import React from 'react';
import styles from './Order.module.css';

const Order = (props) => {

    let list = [];
    for (let key in props.order.ingredients) {
        list.push({
            id: key,
            value: props.order.ingredients[key]
        })
    }
    return(
        <div className={styles.Order}>
            <p>Customer: {props.order.orderData.name}</p>
            <p>Ingredients: 
                {list.map(ing => {
                    return (
                        <span key={ing.id} className={styles.Ingredient}>
                            {ing.id} {ing.value}
                        </span>
                    ) 
                })} 
            </p>
            <p>Price: <strong>USD {props.order.price.toFixed(2)}</strong></p>
            <p>Delivery: &nbsp; 
                <span style={{textTransform: 'capitalize'}}>
                    {props.order.orderData.deliveryMethod}
                </span>
            </p>
        </div>
    )
}

export default Order;