import React from 'react';
import styles from './BuildControl.module.css';

const BuildControl = (props) => (
    <div className={styles.BuildControl}>
        <div className={styles.Label}>{props.label}</div>
        <button className={styles.Less}
            disabled={props.disabled}
            onClick={props.onRemoved}>
            Less
        </button>
        <button className={styles.More}
            onClick={props.onAdded}>
            More
        </button>
    </div>
)

export default BuildControl;