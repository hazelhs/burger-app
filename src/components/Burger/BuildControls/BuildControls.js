import React from 'react';
import styles from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Chesse', type: 'cheese'},
    {label: 'Meat', type: 'meat'}
];

const BuildControls = (props) => (
    <div className={styles.BuildControls}>
        <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(control => (
            <BuildControl 
                key={control.label} 
                label={control.label}
                disabled={props.disabledControl[control.type]}
                onRemoved={()=>props.onRemoveIngredient(control.type)}
                onAdded={()=>props.onAddIngredient(control.type)} />
        ))}

        <button 
            className={styles.OrderButton}
            disabled={!props.purchasable}
            onClick={props.onOrdered}>
            {props.isAuthenticated ? "ORDER NOW" : "LOGIN TO ORDER"}
        </button>
    </div>
)

export default BuildControls;