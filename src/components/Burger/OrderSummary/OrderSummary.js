import React from 'react';
import Button from '../../UI/Button/Button';

class OrderSummary extends React.Component {
    componentWillUpdate() {
        console.log('[OrderSummary] will update');
    }

    render() {
        {const ingredientSummary = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{'textTransform': 'capitalize'}}>{igKey}</span>
                        : {this.props.ingredients[igKey]}
                    </li>
                )
            });

            return(
                <React.Fragment>   
                    <div>
                        <h3>Your Order</h3>
                        <p>A delicious burger with the following ingredients:</p>
                        <ul>
                            {ingredientSummary}
                        </ul>
                        <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
                        <p>Continue to Checkout?</p>
        
                        <Button
                            btnType='Danger' 
                            onButtonClicked={this.props.onCancel}>
                            CANCEL
                        </Button>
                        
                        <Button
                            btnType='Success' 
                            onButtonClicked={this.props.onContinue}>
                            CONTINUE
                        </Button>
                    </div>
                </React.Fragment>
            )
        }
    }
}

export default OrderSummary;