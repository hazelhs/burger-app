import React from 'react';
import styles from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../Sidedrawer/DrawerToggle/DrawerToggle';

const Toolbar = (props) => {
   return(
    <header className={styles.Toolbar}>
         <DrawerToggle hamburgerClicked={props.onHamburgerClicked} />
         <div className={styles.Logo}>
            <Logo/>
         </div>
         <nav className={styles.DesktopOnly}>
            <NavigationItems isAuthenticated={props.isAuthenticated} />
         </nav>
    </header>
   )
}

export default Toolbar;