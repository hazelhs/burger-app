import React from 'react';
import styles from './Sidedrawer.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems'
import Backdrop from '../../UI/Backdrop/Backdrop';


const Sidedrawer = (props) => {
    let attachedClasses = [styles.Sidedrawer, styles.Close];
    if(props.open) {
        attachedClasses = [styles.Sidedrawer, styles.Open];
    }

    return(
       <React.Fragment>   
           <Backdrop 
                show={props.open}
                onBackdropClicked={props.onClosed} />
            <div className={attachedClasses.join(' ')} 
                onClick={props.onClosed}>
                <div className={styles.Logo}>
                    <Logo/>
                </div>
                <nav>
                    <NavigationItems isAuthenticated={props.isAuthenticated} />
                </nav>
            </div>
       </React.Fragment>
    )
}

export default Sidedrawer;