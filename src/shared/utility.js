export const checkValidation = (value, rules) => {

    let validation = {
        isValid: true,
        errorMessage: ''
    };

    if(!rules) {
        return validation;
    }

    if(rules.required) {
        const valid = value.trim() !== '' && validation.isValid;
        if(!valid) {
            validation = {
                isValid: false,
                errMessage: 'This field is required'
            }
            return validation;
        }
    } 

    if(rules.minLength) {
        const valid = value.length >= rules.minLength && validation.isValid;
        if(!valid) {
            validation = {
                isValid: false,
                errMessage: `Minimum length is ${rules.minLength}`
            }
            return validation;
        }
    }

    if(rules.maxLength) {
        const valid = value.length <= rules.maxLength && validation.isValid;
        if(!valid) {
            validation = {
                isValid: false,
                errMessage: `Maximum length is ${rules.minLength}`
            }
            return validation;
        }
    }

    if(rules.isEmail) {
        const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const valid = pattern.test(value) && validation.isValid;

        if(!valid) {
            validation = {
                isValid: false,
                errMessage: 'Email entered is not valid'
            }
            return validation;
        }
    }

    if(rules.isNumeric) {
        const pattern = /^\d+$/;
        const valid = pattern.test(value) && validation.isValid;

        if(!valid) {
            validation = {
                isValid: false,
                errMessage: 'Number is required'
            }
            return validation;
        }
    }

    return validation;
}