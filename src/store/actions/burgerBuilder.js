import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const addIngredient = (name) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredientName: name
    };
};

export const removeIngredient = (name) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredientName: name
    }
}

export const initIngredients = () => async dispatch => {
   try {
        const response = await axios.get('/ingredients.json');
        dispatch({
            type: actionTypes.SET_INGREDIENTS,
            payload: response.data
        });
   } catch (e) {
        dispatch({
            type: actionTypes.FETCH_INGREDIENTS_FAILED
        });
   }
}
