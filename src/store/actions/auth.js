import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token,
        userId
    }
}

export const authFailed = (error) => {
    return {
        type: actionTypes.AUTH_FAILED,
        error: error
    }
}

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = (expTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expTime * 1000);
    }
}

export const auth = (email, password, isSignUp) => async dispatch => {
    try{
        dispatch(authStart());
        console.log('[auth actions] isSignUp? ', isSignUp);
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        }
        const apiKey = "AIzaSyAO_rQVWiiNrXDGfsp5YfdFAl-OVl57l_A";
        let URL = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${apiKey}`;
        if(!isSignUp) {
            URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${apiKey}`
        }
        
        const response = await axios.post(URL, authData);
        console.log('[auth response]' , response);

        const expDate = new Date(new Date().getTime() + (response.data.expiresIn * 1000));
        localStorage.setItem('token', response.data.idToken);
        localStorage.setItem('expirationDate', expDate);
        localStorage.setItem('userId', response.data.localId);
        dispatch(authSuccess(response.data.idToken, response.data.localId));
        dispatch(checkAuthTimeout(response.data.expiresIn));
    } catch (e) {
        console.log(e.response);
        dispatch(authFailed(e.response.data.error));
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if(expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch( checkAuthTimeout((expirationDate.getTime() - new Date().getTime())/1000) );
            }
        }
    }
}