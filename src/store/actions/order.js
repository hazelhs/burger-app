import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT
    }
}

export const purchaseBurgerSuccess = (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData: orderData
    }
}

export const purchaseBurgerFailed = (error) => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAILED,
        error: error
    }
}

export const purchaseBurgerStart = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START
    }
}

export const purchaseBurger = (orderData, token) => async dispatch => {
    try {
        dispatch(purchaseBurgerStart());
        const response = await axios.post('/orders.json?auth=' + token, orderData);
        dispatch(purchaseBurgerSuccess(response.data.name, orderData));
    } catch (e) {
        dispatch(purchaseBurgerFailed(e));
    }
}

export const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START
    }
}

export const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders
    }
}

export const fetchOrdersFailed = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILED,
        error
    }
}

export const fetchOrders = (token, userId) => async dispatch => {
    try {
        dispatch(fetchOrdersStart());

        const queryParams = `?auth=${token}&orderBy="userId"&equalTo="${userId}"`;
        const response = await axios.get('/orders.json' + queryParams);
        const fetchedOrders = [];
        for (let key in response.data) {
            fetchedOrders.push({
                ...response.data[key],
                id: key
            })
        }
        dispatch(fetchOrdersSuccess(fetchedOrders));
   } catch (e) {
        dispatch(fetchOrdersFailed(e));
   }
}