import * as actionTypes from '../actions/actionTypes';

const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    isBuilding: false
}

const INGREDIENT_PRICES = {
    salad: 0.5,
    meat: 1.5,
    cheese: 1.0,
    bacon: 0.8
}

const addIngredient = (state, action) => {
    return {
        ...state,
        ingredients: {
            ...state.ingredients,
            [action.ingredientName]: state.ingredients[action.ingredientName] + 1
        },
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
        isBuilding: true
    }
}

const removeIngredient = (state, action) => {
    return {
        ...state,
        ingredients: {
            ...state.ingredients,
            [action.ingredientName]: state.ingredients[action.ingredientName] - 1
        },
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
        isBuilding: true
    }
}

const fetchIngredients = (state, action) => {
    return {
        ...state,
        ingredients: action.payload,
        totalPrice: 4,
        error: false,
        isBuilding: false
    }
}

const fetchIngredientsFailed = (state, action) => {
    return {
        ...state,
        error: true
    }
}

const burgerBuilder = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT: return addIngredient(state, action);
        case actionTypes.REMOVE_INGREDIENT: return removeIngredient(state, action);
        case actionTypes.SET_INGREDIENTS: return fetchIngredients(state, action);
        case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state, action);  
        default: return state;
    }
}

export default burgerBuilder;