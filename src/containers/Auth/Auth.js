import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import { checkValidation } from '../../shared/utility';

import styles from './Auth.module.css';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';

class Auth extends Component {
    state = {
        isFormValid: false,
        isSignUp: true,
        controls: {
            email: {   
                label: 'Email',
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email'
                },
                value: '' ,
                validation: {
                    required: true,
                    isEmail: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            password: {
                label: 'Password',
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Your Password'
                },
                value: '' ,
                validation: {
                    required: true,
                    minLength: 6
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
        }
    }

    componentDidMount() {
        if(!this.props.isBuildingBurger && this.props.authRedirectPath !== '/') {
            this.props.onSetAuthRedirectPath();
        }
    }

    inputChangedHandler = (e, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: e.target.value,
                validity: checkValidation(e.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        // console.log(updatedControls[controlName].valid);
        
        let isFormValid = true;
        for (let c in updatedControls) {
            isFormValid = updatedControls[c].validity.isValid && isFormValid;
        }
        this.setState({controls: updatedControls, isFormValid});
    }


    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {isSignUp: !prevState.isSignUp}
        })
    }


    submitHandler = (e) => {
        e.preventDefault();
        this.props.onAuthenticate(
            this.state.controls.email.value, 
            this.state.controls.password.value, 
            this.state.isSignUp);
    }


    render() {
        const formElementsArray = [];
        for(let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => {
            return (
                <Input 
                key={formElement.id}
                label={formElement.config.label}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                touched={formElement.config.touched}
                invalid={!formElement.config.validity.isValid}
                errorMessage={formElement.config.validity.errMessage}
                shouldValidate={formElement.config.validation}
                onElementChanged={(e) => this.inputChangedHandler(e, formElement.id)}/>
            )
        });

        if(this.props.loading) {
            form = <Spinner /> 
        }

        let errorMessage = null;
        if(this.props.error) {
            errorMessage = (
                <div style={{
                        backgroundColor: 'salmon', 
                        color: 'white',
                        padding: '5px',
                        fontWeight: 'bold', 
                        border: '1px solid #ccc',
                        boxSizing: 'border-box'}}>
                    Credentials are not correct. Please make sure you enter correct email and password.
                </div>
            )
        }

        let authRedirect = null;
        if(this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        return (
            <div className={styles.Auth}>
                { authRedirect }
                { errorMessage }
                <form onSubmit={this.submitHandler}>
                    { form }
                    <Button disabled={!this.state.isFormValid} 
                            btnType="Success">
                        SUBMIT
                    </Button>
                </form>

                <Button btnType="Danger"
                    onButtonClicked={this.switchAuthModeHandler}>
                    SWITCH TO {this.state.isSignUp ? 'SIGN IN' : 'SIGN UP'}
                </Button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    // console.log('[Auth Component]', state);
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        isBuildingBurger: state.burgerBuilder.isBuilding,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuthenticate: (email, password, isSignUp) => dispatch(actions.auth(email, password, isSignUp)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);