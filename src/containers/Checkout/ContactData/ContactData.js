import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../../axios';
import ErrorHandler from '../../../components/ErrorHandler/ErrorHandler';
import * as orderActions from '../../../store/actions/index';
import { checkValidation } from '../../../shared/utility';

import styles from './ContactData.module.css';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';

class ContactData extends Component {
    state = {
        formIsValid: false,
        orderForm: {
            name: {
                label: 'Name',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '' ,
                validation: {
                    required: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            email: {
                label: 'Email',
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            street: {
                label: 'Street',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            zipCode: {
                label: 'ZIP Code',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ZIP Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 4,
                    maxLength: 4,
                    isNumeric: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            country: {
                label: 'Country',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                validity: {
                    isValid: false,
                    errMessage: ''
                },
                touched: false
            },
            deliveryMethod: {
                label: 'Delivery Method',
                elementType: 'select',
                elementConfig: {
                    options:[
                        {
                            value: 'fastest',
                            displayValue: 'Fastest'
                        },
                        {
                            value: 'cheapest',
                            displayValue: 'Cheapest'
                        }
                    ]
                },
                value: 'cheapest',
                validation: {},
                validity: {
                    isValid: true,
                    errMessage: ''
                },
            }
        }
    }

    orderHandler = async(e) => {
        e.preventDefault();

        const formData = {};
        for(let formElementIdentifier in this.state.orderForm) {
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
        }
        //calculate price in the server
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            orderData: formData,
            userId: this.props.userId
        }
        
        this.props.onOrderBurger(order, this.props.token);
    }

    

    inputChangedHandler = (e, inputIdentifier) => {
        const updatedOrderForm = {
            ...this.state.orderForm
        };
        const updatedFormElement = {
            ...updatedOrderForm[inputIdentifier]
        };

        updatedFormElement.value = e.target.value;
        updatedFormElement.validity = checkValidation(updatedFormElement.value, updatedFormElement.validation)
        updatedFormElement.touched = true;
        // console.log(updatedFormElement);
        updatedOrderForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifier].validity.isValid && formIsValid;
        }
        this.setState({orderForm: updatedOrderForm, formIsValid});
    }

    render() {
        const formElementsArray = [];
        for(let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        label={formElement.config.label}
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        touched={formElement.config.touched}
                        invalid={!formElement.config.validity.isValid}
                        errorMessage={formElement.config.validity.errMessage}
                        shouldValidate={formElement.config.validation}
                        onElementChanged={(e) => this.inputChangedHandler(e, formElement.id)} />
                ))}

                <Button 
                    disabled={!this.state.formIsValid}
                    onButtonClicked={this.orderHandler} 
                    btnType="Success">ORDER
                </Button>
            </form>
        )
        if(this.props.loading) {
            form = <Spinner />;
        }

        return(
            <div className={styles.ContactData}>
                <h4>Enter your contact data</h4>
                { form }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchOnProps = dispatch => {
    return {
        onOrderBurger: (orderData, token) => dispatch(orderActions.purchaseBurger(orderData, token))
    }
}

export default connect(mapStateToProps, mapDispatchOnProps)(ErrorHandler(ContactData, axios))