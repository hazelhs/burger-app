import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import ChekoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from './ContactData/ContactData';


class Checkout extends Component {

    checkoutCancelledHandler = () => {
        this.props.history.goBack();
    }

    checkoutContinuedHandler = () => {
        this.props.history.replace(this.props.match.path + '/details');
    }

    render() {
        let summary = <Redirect to="/" />
        if(this.props.ingredients) {
            const purchasedRedirect = this.props.purchased ? <Redirect to="/" /> : null
            summary = (
                <React.Fragment>
                    { purchasedRedirect } 
                    <ChekoutSummary 
                        ingredients={this.props.ingredients} 
                        onCheckoutCancelled={this.checkoutCancelledHandler} 
                        onCheckoutContinued={this.checkoutContinuedHandler} />

                    <Route 
                        path={this.props.match.path + '/details'} 
                        component={ContactData}
                    />
                </React.Fragment>
            )
        }
        return summary;
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        purchased: state.order.purchased
    }
}

export default connect(mapStateToProps)(Checkout);