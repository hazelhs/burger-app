import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';

import axios from '../../axios';
import Spinner from '../../components/UI/Spinner/Spinner';
import ErrorHandler from '../../components/ErrorHandler/ErrorHandler';

class BurgerBuilder extends Component {
    state = {
        purchasing: false
    }

    componentDidMount() {
       this.props.onInitIngredients();
    }

    updatePurchaseState() {
        const sum = Object.keys(this.props.ingredients)
            .map(igKey => {
                return this.props.ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

        return sum > 0;
    }

    purchaseHandler = () => {
        if(this.props.isAuthenticated) {
            this.setState({purchasing: true});
        } else {
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push("/auth");
        }
    }

    cancelPurchaseHandler = () => {
        this.setState({purchasing: false});
    }

    continuePurchaseHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disabledInfo = {
            ...this.props.ingredients
        };

        for(let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let renderOrderSummary = null;
        let renderBurger = this.props.httpError 
            ? <p>Ingredients can't be loaded!!</p>
            : <Spinner />;

        if(this.props.ingredients) {
            renderBurger = (
                <React.Fragment>
                    <Burger ingredients={this.props.ingredients} /> 
                    <BuildControls 
                        isAuthenticated={this.props.isAuthenticated}
                        price={this.props.totalPrice}
                        purchasable={this.updatePurchaseState()}
                        onOrdered={this.purchaseHandler}
                        disabledControl={disabledInfo}
                        onAddIngredient={this.props.onIngredientAdded} 
                        onRemoveIngredient={this.props.onIngredientRemoved} />
                </React.Fragment>
            )

            renderOrderSummary = <OrderSummary 
                price={this.props.totalPrice}
                onCancel={this.cancelPurchaseHandler}
                onContinue={this.continuePurchaseHandler}
                ingredients={this.props.ingredients}/>
        }

        return(
            <React.Fragment>
                <Modal show={this.state.purchasing}
                        modalClosed={this.cancelPurchaseHandler}>
                    { renderOrderSummary }
                </Modal>
                { renderBurger }
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        httpError: state.burgerBuilder.error,
        isAuthenticated: state.auth.token !== null
    }
}
 
const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingredientName) => dispatch(actions.addIngredient(ingredientName)),
        onIngredientRemoved: (ingredientName) => dispatch(actions.removeIngredient(ingredientName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ErrorHandler(BurgerBuilder, axios));